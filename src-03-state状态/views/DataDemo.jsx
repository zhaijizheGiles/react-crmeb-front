import React, { Component } from 'react'
/**
 *  一、React中的状态
 *  1、React的组件分为类组件和函数组件，类组件是有状态的组件，函数组件是无状态的组件，React16.8之之后给函数组件引入了hooks
 *   hooks中提供了对状态的操作
 *  2、类组件中的状态
 *  类组件中的状态分为两种
 *  1）来自类内部的状态：state
 *  2) 来自外部类状态：props
 *  二、关于state的操作步骤
 *  第1步：定义state的初始值
 *  有两种方式
 *  1）在构造函数中定义
    constructor(){
        super()
        this.state={
            count:0
        }
    }
    2）定义在类中
    state={
      count:0
    }
    2、在render中使用state
    this.state.count
    3、更新state的状态
    使用this.state.count=this.state.count+1不能让页面自动变化，解决方法有两种
    第1种方法(推荐方法)
    this.setState({
        count:更新后的值
    })
    第2种方法(不推荐)
    this.state.count=this.state.count+1
    this.forceUpdate()
   三、简化render中state.xx的操作
   可以直接在render中使用ES6的对象解构的方式来完成
   render(){
     const {xx}=this.state
   }


 */
export default class DataDemo extends Component {
//   constructor(){
//     super()
//     this.state={
//         count:0
//     }
//   }
  state={
    count:0
  }
  render() {
    const {count}=this.state
    return (
      <div>
         <h1>计数器</h1>
         <div>{count}</div>
         <div>
            <button onClick={()=>{
               //this.state.count=this.state.count+1
               this.setState({
                 count:count+1
               })
             //this.forceUpdate()
               console.log('count',count);
            }}>+</button>
         </div>
      </div>
    )
  }
}
