import React, { Component } from 'react'
import './ShopCart.scss'
export default class ShopCart extends Component {
  constructor(){
    super()
    this.state={
        shopcartList:[
            {
              pid: '1001',
              pname: '欧莱雅男士护肤',
              price: 38,
              num: 1
            },
            {
              pid: '1002',
              pname: 'OLAY女士防皱润肤露',
              price: 108,
              num: 1
            },
            {
              pid: '1003',
              pname: '自然堂女士护肤',
              price: 108,
              num: 2
            },
            {
              pid: '1004',
              pname: '兰蔻香水',
              price: 1038,
              num: 1
            },
            {
              pid: '1005',
              pname: '大宝SOD,每个人选择',
              price: 8,
              num: 1
            }
          ]
    }
  }
  getTotal(){
    return this.state.shopcartList.reduce((prev,cur)=>prev+cur.price*cur.num,0)
  }
  changeNum(op,index){
    switch(op){
        case '-':
            if(this.state.shopcartList[index].num<=0){
                alert('该商品不能再减了')
            }else{
               const newShopCartList=[...this.state.shopcartList]
               newShopCartList[index].num--
               this.setState({
                shopcartList:newShopCartList
               })
            }
           
            break
        case '+':
            const newShopCartList=[...this.state.shopcartList]
            newShopCartList[index].num++
            this.setState({
             shopcartList:newShopCartList
            })
            break;
    }
  }
  removeProduct(index){
    this.state.shopcartList.splice(index,1)
    this.setState({
        shopcartList:this.state.shopcartList
    })
  }
  render() {
    const {shopcartList}=this.state
    return (
      <div>
        <table>
            <thead>
                <tr>
                    <th>编号</th>
                    <th>商品名称</th>
                    <th>商品价格</th>
                    <th>数量</th>
                    <th>小计</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody>
                {
                    shopcartList.map((item,index)=><tr key={item.pid}>
                        <td>{item.pid}</td>
                        <td>{item.pname}</td>
                        <td>{item.price}</td>
                        <td>{item.num}</td>
                        <td>
                            <button onClick={()=>{this.changeNum('-',index)}}>-</button>
                            <span>{item.price*item.num}</span>
                            <button onClick={()=>{this.changeNum('+',index)}}>+</button>
                        </td>
                        <td>
                            <button onClick={()=>{this.removeProduct(index)}}>删除</button>
                        </td>
                    </tr>)
                }
            </tbody>
            <tfoot>
                <tr>
                    <td colSpan={6}>
                        总价:{this.getTotal()}
                    </td>
                </tr>
            </tfoot>
        </table>
      </div>
    )
  }
}
