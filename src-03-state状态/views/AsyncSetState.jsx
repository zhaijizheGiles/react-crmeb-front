import React, { Component } from 'react'
import ReactDOM from 'react-dom'
/* 
    setState是同步还是异步的问题
    1、合成事件不管是什么版本都是异步的
    2、react18之前对于原生事件、定时器(间隔定时器、延时定时器)任务是同步的
       react18之后对于原生事件、定时器(间隔定时器、延时定时器)任务是异步的
    3、在react18之后，如果要向将合成事件的步骤变成同步的，可以使用
      1) ReactDOM.flushSync()方法来完成，这种调用一次执行一次
      2）this.setState(回调函数1，回调函数2)的方式来完成，回调函数2是运行在回调函数1之后的，但是多次setState只执行一次
 


*/
export default class AsyncSetState extends Component {

    componentDidMount() {
        //原生事件
        document.querySelector('#btn').addEventListener('click', () => {
            console.log('1、', this.state.num);
            this.setState({
                num: this.state.num + 1
            })
            console.log('2、', this.state.num);
        })
        //定时器
        // window.setTimeout(()=>{
        //      console.log('giles1、',this.state.num);
        //       this.setState({
        //         num:this.state.num+1
        //       })
        //       console.log('giles2、',this.state.num);
        // },0)
        // window.setInterval(() => {
        //     console.log('giles-1、', this.state.num);
        //     this.setState({
        //         num: this.state.num + 1
        //     })
        //     console.log('giles-2、', this.state.num);
        // }, 5000)
    }


    state = {
        num: 0
    }
    increment() {
       
        ReactDOM.flushSync(()=>{
            //修改操作
            this.setState({
                num: this.state.num + 1
            })
           
        })
        console.log('1、', this.state.num);
        
        ReactDOM.flushSync(()=>{
            //修改操作
            this.setState({
                num: this.state.num + 1
            })
        })
        console.log('2、', this.state.num);
        
        // this.setState(()=>{
        //     return{
        //         num:this.state.num+1
        //     }
        // },()=>{
        //     console.log('1、',this.state.num);
        // })
    }
    render() {
        const { num } = this.state
        return (
            <div>
                <h1>{num}</h1>
                <button onClick={this.increment.bind(this)}>合成事件</button>
                <button id='btn'>原生事件</button>
            </div>
        )
    }
}
