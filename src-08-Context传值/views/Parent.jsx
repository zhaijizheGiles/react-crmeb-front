import React, { Component } from 'react'
import Child from '@/views/Child'
import MyContext from '../utils/MyContext'
export default class Parent extends Component {

  render() {
    return (
      <div style={{
        width:'100px',
        height:'100px',
        backgroundColor:'springgreen',
        display:'flex',
        justifyContent:'center',
        alignItems:'center'
      }}>
        <Child></Child>
      </div>
    )
  }
}
