import React, { Component } from 'react'
import MyContext from '@/utils/MyContext'
export default class Child extends Component {
  render() {
    return (
      <MyContext.Consumer>
         {
           arg=>(
                <div style={{
                  width:'80px',
                  height:'80px',
                  backgroundColor:'tomato',
                  display:'flex',
                  justifyContent:'center',
                  alignItems:'center'
                }}>
                  {arg}
                </div>          
              )
           }
      </MyContext.Consumer>
    )
  }
}
