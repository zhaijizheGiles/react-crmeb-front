import React, { Component } from 'react'
import Parent from './views/Parent'
import MyContext from './utils/MyContext'
/*
  跨级组件通信的实现步骤
  1、在src/utils问价下创建MyContext.js
  2、在组件传递的提供一方使用Context.Provider来实现数据传递，使用<Provider>标签将提供一方JSX给包裹起来
  3、在接受组件的一方使用Context.Customer来实现接受数据，使用<Customer>标签将接受的一方的JSX给包裹起来
*/
export default class App extends Component {
  state={
    msg:'我是来自爷爷的数据'
  }
  render() {
    return (
      <MyContext.Provider value={this.state.msg}>
          <div style={{
            width:'150px',
            height:'150px',
            backgroundColor:'skyblue',
            display:'flex',
            justifyContent:'center',
            alignItems:'center'
          }}>
            <Parent></Parent>
          </div>
      </MyContext.Provider>
    )
  }
}
