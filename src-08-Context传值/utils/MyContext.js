//从react核心包中导入createContext方法，该方法可以用来创建Context对象
import {createContext} from 'react'
//调用createContext方法完成Context对象的创建
const mContext=createContext()
//使用ES6默认导出的方式将这个对象导出
export default mContext