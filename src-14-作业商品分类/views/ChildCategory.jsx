import React, { Component } from 'react'
import './ChildCategory.scss'
export default class ChildCategory extends Component {
  constructor(props){
    super(props)
    this.state={
      list:[]
    }
  }
  UNSAFE_componentWillReceiveProps(nextProps,nextState){
    this.setState({
      list:nextProps.childList
    })
  }
  render() {
    const {list}=this.state
    return (
      <div className='box'>
         {  
            list.map(item=><div key={item.id} className='pro-item'>
                <div className='pro-item-header'>
                    <img src={item.extra} alt="" />
                </div>
                <div>
                  {item.name}
                </div>
            </div>)
         }
      </div>
    )
  }
}
