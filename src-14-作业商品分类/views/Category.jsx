import React, { Component } from 'react'
import api from '@/api'
import './Category.scss'
import ChildCategory from '@/views/ChildCategory'
import NotFoundChildCategory from '@/views/NotFoundChildCategory'
export default class Category extends Component {
  constructor(props){
    super(props)
    this.state={
        firstCategory:[],
        childCategory:[]
    }
  }
  componentDidMount(){
    this.getCategoryApi()

  }
  getCategoryApi=async()=>{
    const result=await api.category.getCategory()
    this.setState({
        firstCategory:result.data.data.map(item=>({id:item.id,name:item.name,child:item.child})),
        childCategory:result.data.data[0].child
    })
  }
  getCategoryChild=(index)=>{
    console.log(this.state.firstCategory[index].child);
    this.setState({
        childCategory:this.state.firstCategory[index].child
    })
  }
  render() {
    const {firstCategory,childCategory}=this.state
    return (
      <div  className='container'>
         <div className='header'>
            {firstCategory.map((item,index)=><div className="item" key={item.id} onClick={()=>{this.getCategoryChild(index)}}>{item.name}</div>)}
         </div>
         <div>
            {childCategory?<ChildCategory childList={childCategory}/>:<NotFoundChildCategory/>}
         </div>
      </div>
    )
  }
}
