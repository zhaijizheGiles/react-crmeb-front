import React from 'react'
export function withMouse(WrapperComponent){
    class Mouse extends React.Component{
        state={
            x:0,
            y:0
        }
        hanldeMoveMouse=(e)=>{
            this.setState({
                x:e.clientX,
                y:e.clientY
            })
        }
        componentDidMount(){
            window.addEventListener('mousemove',this.hanldeMoveMouse)
        }
        componentWillUnmount(){
            window.removeEventListener('mousemove',this.hanldeMoveMouse)
        }
        render(){
            return (<>
                <WrapperComponent {...this.state} {...this.props}></WrapperComponent>
            </>)
        }
    }
    return Mouse
}

