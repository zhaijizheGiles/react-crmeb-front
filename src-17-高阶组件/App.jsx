import React, { Component } from 'react'
import {withMouse} from '@/components/withMouse'
// import Card from './views/Card'
import Ball from './views/Ball'
//调用高阶组件
const HigherBall=withMouse(Ball)
export default class App extends Component {
  render() {
    return (
      <div>
        <HigherBall a={123}></HigherBall>
      </div>
    )
  }
}
