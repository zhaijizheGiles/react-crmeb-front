import React, { Component } from 'react'

export default class Card extends Component {
  
  render() {
    return (
      <div style={{
        width:'300px',
        height:'300px',
        background:'#ccc',
        boxShadow:'10px 10px 10px #999'
      }}>
        <span>
            坐标:({this.props.x},{this.props.y})
        </span>
      </div>
    )
  }
}
