import React, { Component,createRef } from 'react'
export default class Ball extends Component {
  constructor(props){
    super(props)
    this.boxEle=createRef()
  }
  UNSAFE_componentWillReceiveProps(nextProps,nextState){
    console.log('aaa',nextProps);
    this.boxEle.current.style.top=nextProps.y+"px"
    this.boxEle.current.style.left=nextProps.x+"px"
  }
  render() {
    return (
      <div style={{
        position:'relative'
      }}>
          <div style={{
            width:'40px',
            height:'40px',
            backgroundColor:'springgreen',
            borderRadius:'50%',
            position:'absolute',
            top:'100px',
            left:'300px'
          }} ref={this.boxEle}></div>
      </div>
    )
  }
}
