import React, { useEffect, useState } from 'react'

export default function App() {
  const [count,setCount]=useState(0)
  const [num,setNum]=useState(100)
  //第2个参数没有：运行加载阶段和运行阶段操作
  //第2个参数为[]数组，只在加载阶段运行
  //第3个参数为非空数组，数组中写上那个变量，哪个变量就会被监视
  //在类组件中在销毁组件之前清空定时器
  //useEffect第1个参数是一个回调函数，回调函数的返回值是一个回调函数，在这个回调函数就能做组件的销毁

  /*
    定时器在hooks函数的操作
    首先：useEffect第1个参数中使用间隔定时器对state状态的更新
    其次：useEffect第1个参数(回调函数)的返回值(回调函数)中完成对该定时器的销毁
    最后：切记这里useEffect第2个参数没有的
  */
 /*
 
  总结：useEffect的主要作用是完成副作用操作
  1、useEffect第2个参数为[]数组，一般完成对后端api接口的调用，完成获取数据使用的
  2、useEffect没有第2个参数，有第1个参数，第1个参数返回值(回调函数)销毁资源，一般用于定时器的操作
  3、useEffect有第2个参数，但是第2个参数是有选择性的，这种主要完成对某个状态的监视
 */
  
  useEffect(()=>{
    let timers=window.setInterval(()=>{
       console.log('----------------------',count);
       setCount(count+1)
    },1000)
    return()=>{
      window.clearInterval(timers)
    }
  })
  return (
    <div>
      <div>
        <h1>计数器</h1>
        <div>{count}</div>
        <button onClick={()=>setCount(count+1)}>+</button>
      </div>
      <div>
         <h1>减法器</h1>
         <div>{num}</div>
         <button onClick={()=>setNum(num-1)}>-</button>
      </div>
      <hr />
      <h1>{count}</h1>
    </div>
  )
}
