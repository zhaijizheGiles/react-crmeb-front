import React, { Component } from 'react'
import './LessionItem.scss'
import PropType from 'prop-types'
export default class LessionItem extends Component {
  static defaultProps={
    browserNum:'0'
  }
  static propTypes={
   browserNum:PropType.string.isRequired
  }
  constructor(props){
    super(props)
    this.incrementBrowserNum=this.incrementBrowserNum.bind(this)
  }
  incrementBrowserNum(){
      //this.props.browserNum++
      this.props.teacher.teacherAvatar="http://old.woniuxy.com/page/img/head_picture/0.jpg"
      this.forceUpdate()
      // this.props.teacher={
      //    teacherAvatar:'http://old.woniuxy.com/page/img/head_picture/0.jpg',
      //    teacherName:'张三'
      // }
  }
  selectLession=(arg)=>{
    this.props.callback(arg)
  }
  render() {
    const {converImagPath,lessionName,lessionDesc,lessionTimes,lessionType,teacher:{teacherName,teacherAvatar="http://old.woniuxy.com/page/img/head_picture/0.jpg"},browserNum}=this.props
    return (
      <div className='lession-item-container'>
         <div className='lession-item-header'>
            <img className="lession-item-header-img" src={converImagPath} alt="" />
         </div>
         <div className='lession-item-body'>
            <div className='body-one'>
               {lessionName}
            </div>
            <div className='body-two'>
               {lessionDesc}
            </div>
            <div className='body-three'>
                <div>{lessionTimes}</div>
                <div>{lessionType}</div>
            </div>
         </div>
         <div className='lession-item-footer'>
            <div className='lession-item-footer-left'>
                <img className='avatar' src={teacherAvatar} alt="" />
                <span>{teacherName}</span>
            </div>
            <div className='lession-item-footer-right'>{browserNum}</div>
            <div className='lession-item-footer-right1'>
               <button onClick={this.incrementBrowserNum}>浏览</button>
               <button onClick={()=>{
                  this.selectLession(lessionName)
               }}>选课</button>
            </div>
         </div>
      </div>
    )
  }
}
/*
LessionItem.defaultProps={
   "teacher.teacherAvatar":"http://old.woniuxy.com/page/img/head_picture/0.jpg",
   browserNum:0
}
*/
// LessionItem.propTypes={
//    browserNum:PropType.number.isRequired
// }

