import React, { Component } from 'react'
import './LessionList.scss'
import LessionItem from './LessionItem';
export default class LessionList extends Component {
  constructor(props){
    super(props)
  }
  state={
    lessionList:[]
  }
  getMsg=(arg)=>{
   
    this.state.lessionList.push(arg)
    this.setState(()=>{
      return{
        lessionList:this.state.lessionList
      }
    },()=>{
      console.log('this',this.state.lessionList);
    })
  }
  render() {
    const {name,theme,lessions}=this.props
    return (
      <div className='lessionList-container'>
         <div className='lessionList-left' style={{backgroundColor:theme}}>
            {name}<br/>
            {this.state.lessionList.length}
         </div>
         <div className='lessionList-right'>
            {
                lessions.filter((item,index)=>index<4).map((item,index)=><LessionItem key={index} {...item} callback={this.getMsg}></LessionItem>)
            }
         </div>
      </div>
    )
  }
}
