import React, { Component } from 'react'
import Home from './views/Home'

export default class App extends Component {
  render() {
    return (
      <div>
        <Home/>
      </div>
    )
  }
}
