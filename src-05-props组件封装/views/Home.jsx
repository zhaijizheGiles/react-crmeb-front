import React, { Component } from 'react'
import LessionList from '@/components/LessionList'
export default class Home extends Component {
  state = {
    selectedLessionList:[],
    list:[
      {
        name:'Java全栈学习路由',
        theme:'#00CEFF',
        lessions: [
          {
            converImagPath: 'http://old.woniuxy.com/page/image/courseIcon/major1/course20.jpg',
            lessionName: 'SpringCloud',
            lessionDesc: '知识要点：基于SpringCloud微服务架构开发实战',
            lessionTimes: '13小时',
            lessionType: '限时免费',
            teacher: {
              teacherName: '韩凌',
              teacherAvatar: 'http://old.woniuxy.com/page/image/userIcon/teacherIcon/hanling.jpg',
            },
            browserNum: '259',
            callback:(arg)=>{
              this.state.selectedLessionList.push(arg);
              this.setState(()=>{
                return {
                  selectedLessionList:this.state.selectedLessionList
                }
              },()=>{
                console.log('课程列表:',this.state.selectedLessionList);
              })
            }
          },
          {
            converImagPath: 'http://old.woniuxy.com/page/image/courseIcon/major1/course2.jpg',
            lessionName: 'Java面向对象',
            lessionDesc: '知识要点：Java面向对象，封装，继承，多态，基本设计原则，类加载机制，常用API对象',
            lessionTimes: '30小时',
            lessionType: '限时免费',
            teacher: {
              teacherName: '潘光华',
              teacherAvatar: 'http://old.woniuxy.com/page/image/userIcon/teacherIcon/panguanghua.jpg',
            },
            browserNum: '1259',
            callback:(arg)=>{
              this.state.selectedLessionList.push(arg);
              this.setState(()=>{
                return {
                  selectedLessionList:this.state.selectedLessionList
                }
              },()=>{
                console.log('课程列表:',this.state.selectedLessionList);
              })
            }
          },
          {
            converImagPath: 'http://old.woniuxy.com/page/image/courseIcon/major1/course220.jpg',
            lessionName: 'Redis',
            lessionDesc: '知识要点：Redis缓存数据库的应用、Java基于Redis的应用开发',
            lessionTimes: '16小时',
            lessionType: '限时免费',
            teacher: {
              teacherName: '韩凌',
              teacherAvatar: 'http://old.woniuxy.com/page/image/userIcon/teacherIcon/hanling.jpg',
            },
            browserNum: '1259'
          },
          {
            converImagPath: 'http://old.woniuxy.com/page/image/courseIcon/major1/course26.jpg',
            lessionName: 'Java设计模式',
            lessionDesc: '知识要点：面向对象设计原则、UML、设计模式、企业级常见设计模式解析课程目标：掌握7大设计原则，掌握UML建模，掌握常用设计模式了解其它设计模式',
            lessionTimes: '16小时',
            lessionType: '限时免费',
            teacher: {
              teacherName: '高鹏',
              teacherAvatar: 'http://old.woniuxy.com/page/image/userIcon/teacherIcon/gaopeng.jpg',
            },
            browserNum: '251'
          },
          {
            converImagPath: 'http://old.woniuxy.com/page/image/courseIcon/major1/course252.jpg',
            lessionName: 'Linux操作系统',
            lessionDesc: '知识要点：Linux安装配置，文件目录操作，VI命令，管理，用户与权限，环境部署', 
            lessionTimes: '21小时',
            lessionType: '限时免费',
            teacher: {
              teacherName: '高鹏',
              teacherAvatar: 'http://old.woniuxy.com/page/image/userIcon/teacherIcon/gaopeng.jpg',
            },
            browserNum: '251'
          },
          
        ]
       },
       {
        name:'Java高级架构师学习路线',
        theme:'#F97AE5',
        lessions: [
          {
            converImagPath: 'http://old.woniuxy.com/page/image/courseIcon/major1/course20.jpg',
            lessionName: 'SpringCloud',
            lessionDesc: '知识要点：基于SpringCloud微服务架构开发实战',
            lessionTimes: '13小时',
            lessionType: '限时免费',
            teacher: {
              teacherName: '韩凌',
              teacherAvatar: 'http://old.woniuxy.com/page/image/userIcon/teacherIcon/hanling.jpg',
            },
            browserNum: '259'
          },
          {
            converImagPath: 'http://old.woniuxy.com/page/image/courseIcon/major1/course2.jpg',
            lessionName: 'Java面向对象',
            lessionDesc: '知识要点：Java面向对象，封装，继承，多态，基本设计原则，类加载机制，常用API对象',
            lessionTimes: '30小时',
            lessionType: '限时免费',
            teacher: {
              teacherName: '潘光华',
              teacherAvatar: 'http://old.woniuxy.com/page/image/userIcon/teacherIcon/panguanghua.jpg',
            },
            browserNum: '1259'
          },
          {
            converImagPath: 'http://old.woniuxy.com/page/image/courseIcon/major1/course220.jpg',
            lessionName: 'Redis',
            lessionDesc: '知识要点：Redis缓存数据库的应用、Java基于Redis的应用开发',
            lessionTimes: '16小时',
            lessionType: '限时免费',
            teacher: {
              teacherName: '韩凌',
              teacherAvatar: 'http://old.woniuxy.com/page/image/userIcon/teacherIcon/hanling.jpg',
            },
            browserNum: '1259'
          },
          {
            converImagPath: 'http://old.woniuxy.com/page/image/courseIcon/major1/course26.jpg',
            lessionName: 'Java设计模式',
            lessionDesc: '知识要点：面向对象设计原则、UML、设计模式、企业级常见设计模式解析课程目标：掌握7大设计原则，掌握UML建模，掌握常用设计模式了解其它设计模式',
            lessionTimes: '16小时',
            lessionType: '限时免费',
            teacher: {
              teacherName: '张华'
            }
          },
          {
            converImagPath: 'http://old.woniuxy.com/page/image/courseIcon/major1/course252.jpg',
            lessionName: 'Linux操作系统',
            lessionDesc: '知识要点：Linux安装配置，文件目录操作，VI命令，管理，用户与权限，环境部署', 
            lessionTimes: '21小时',
            lessionType: '限时免费',
            teacher: {
              teacherName: '高鹏',
              teacherAvatar: 'http://old.woniuxy.com/page/image/userIcon/teacherIcon/gaopeng.jpg',
            },
            browserNum: '251'
          },
          
        ]
       }
    ]

 
  }
  render() {
    const { list} = this.state
    return (
      <div>
        {/* <LessionItem
          converImagPath={this.state.converImagPath}
          lessionName={this.state.lessionName}
          lessionDesc={this.state.lessionDesc}
          lessionTimes={this.state.lessionTimes}
          lessionType={this.state.lessionType}
          teacherName={this.state.teacherName}
          teacherAvatar={this.state.teacherAvatar}
          browserNum={this.state.browserNum}></LessionItem> */}
         {
          list.map((item,index)=><LessionList key={index} {...item}></LessionList>)
         }
         <div style={{width:'100px',height:'100px',backgroundColor:'yellow'}}>
            {this.state.selectedLessionList.length}
         </div>
      </div>
    )
  }
}
