### 一、react目录结构
#### 1、目录结构的说明
- node_modules:第三方的依赖包
- public:资源
- src:源代码
    - index.js:项目的入口文件
    - views:页面组件
    - components:自定义组件
    - store:状态机
    - router:路由配置文件
    - api:网络请求的接口
    - assets:资源(会被webpack打包的)
    - utils:工具包
    - App.js:项目的根组件
#### 2、index.js入口文件说明
```js
//导入react-dom包
import ReactDOM from 'react-dom'
/*
    使用ReactDOM.createRoot函数创建root对象
    参数说明：要渲染到的真实DOM节点
*/
const root=ReactDOM.createRoot(document.querySelector('#root'))
//使用root对象中的render方法，将虚拟DOM渲染到页面中来
root.render(<h1>Hello React!</h1>)
```
#### 3 创建组件
在React中创建组件有两种形式
- 类组件
- 函数组件：使用普通函数或者箭头函数创建的组件
创建的组件的步骤
- 创建函数或者类组件并使用export default的方式将其导出
- 在要引入的文件中(index.js)中使用import xx from './App'
- 使用`<组件名></组件名>`进行使用
问题：在导入组件的使用只能使用相对路径，如果路径较为复杂，那么使用起来就很麻烦
#### 4、JSX语法
##### JSX的要求
- JSX必须要有一个根元素
为了保证JSX必须要有一个根节点的要求，我们必须要在外面写一个div容器，显然这个div容器是多余的
但是如果不写就会报语法错误，如果要完成一个根容器，但是又不在真实DOM生成的时候，产生div标签，做法又两个
1、使用空标签来完成`<></>`
2、使用<Fragment>来完成
1)、导入Fragment
2)、 直接引用
- 如果元素没有子元素，直接`<元素/>`
- 为了避免分号陷阱，一般JSX使用`()`括起来
- 属性使用驼峰式命名法来完成
#### JSX的语法
JSX的表达式是写在`{}`,这种写法称为胡子语法
#### JSX中如何操作样式
React的样式操作做分为两种
1、行内样式(内联样式)
style={{key:value}},这里边key的属性值符合驼峰式命名法
- 优缺点：优点：具有样式隔离特性；style样式的优先级比较高 缺点：样式和结构混在一起，可读性扩展性等都有所降低
2、类样式
#### JSX中引入本地图片
由于src/assets文件夹下的图片都会被webpack进行转换和压缩
所以他的引入方式有两种
1、直接使用ES6的import方式进行导入，然后进行使用
2、使用Node.js的commonjs规范的方式引入(require)的方式引入

