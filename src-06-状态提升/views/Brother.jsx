import React, { Component } from 'react'

export default class Brother extends Component {
  sendMoneyToParent=()=>{
    this.props.sendParentFun('哥哥给妹妹200元')
   console.log(this);
  }  
  render() {
    return (
      <div style={{
        flexGrow:1,
        backgroundColor:'skyblue'
      }}>
        <h1>我是哥哥</h1>
        <button onClick={()=>{this.sendMoneyToParent()}}>发送200元</button>
      </div>
    )
  }
}
