import React, { Component } from 'react'
import Brother from './Brother'
import Sister from './Sister'

export default class Parent extends Component {
  state={
    msg:''
  }
  getBrotherMsg=(arg)=>{
    this.setState(()=>{
        return{
            msg:arg
        }
    },()=>{
        console.log('aaa',this.state.msg);
    })
    
  }
  render() {
    return (
      <div style={{
        width:'500px',
        height:'500px',
        backgroundColor:'yellow',
        display:'flex'
      }}>
          <Brother sendParentFun={this.getBrotherMsg}></Brother>
           <Sister msg={this.state.msg}></Sister>
      </div>
    )
  }
}
