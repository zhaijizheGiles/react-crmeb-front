import React, { useEffect,useMemo,useRef,useState } from 'react'
import * as echarts from 'echarts'
export default function Charts() {
  const boxRef=useRef()
  const [list,setList]=useState([39,23,15,16,29])
  const options=useMemo(()=>{
    return {
        title:{
            text:'TOP商品排名'
        },
        xAxis:{
            data:['手机','电视','电饭锅','电脑','平板']
        },
        yAxis:{},
        series:[
            {
                type:'bar',
                data:list
            }
        ]
    }
  },[list])
  useEffect(()=>{
    let mychart=echarts.init(boxRef.current)
    mychart.setOption(options)
  },[])
  return (
    <div>
        <div style={{
            width:'500px',
            height:'500px',
            backgroundColor:'#ccc'
        }}
        ref={boxRef}></div>
    </div>
  )
}
