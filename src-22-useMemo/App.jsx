import React from 'react'
import AddCalc from './components/AddCalc'
import ShopCartList from './components/ShopCartList'
import Charts from './components/Charts'

export default function App() {
  return (
    <div>
      <AddCalc></AddCalc>
      <hr />
      <ShopCartList></ShopCartList>
      <hr />
      <Charts></Charts>
    </div>
  )
}
