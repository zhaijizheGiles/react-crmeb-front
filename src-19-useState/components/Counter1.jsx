import React,{useState} from 'react'

export default function Counter1(props) {
  const {initval}=props
  const [num,setNum]=useState(()=>{
    return initval
  })
  return (
    <div>
        <h1>计数器1</h1>
        <h1>{num}</h1>
        <button onClick={()=>{
            setNum(num+1)
        }}>+</button>
    </div>
  )
}
