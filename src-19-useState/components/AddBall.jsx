import React,{useState} from 'react'

export default function AddBall() {
  const [list,setList]=useState([1,2,3])
  return (
    <div style={{display:'flex'}}>
        {
            list.map((item,index)=><div key={index} style={{
                width:'100px',
                height:'100px',
                background:'orange',
                borderRadius:'50%',
                display:'flex',
                justifyContent:'center',
                alignItems:'center',
                fontSize:'38px',
                color:'white',
                margin:'10px'
              }}>{item}</div>)
        }
          <div style={{
            width:'100px',
            height:'100px',
            border:'3px dashed #ccc',
            borderRadius:'50%',
            display:'flex',
            justifyContent:'center',
            alignItems:'center',
            fontSize:'38px',
            color:'#ccc',
            margin:'10px'
          }} onClick={()=>{
            list.push(4)
            setList([...list])
          }}>+</div>
    </div>
  )
}
