import React,{useState} from 'react'
/*
    一、hooks的概述
    hooks中很多函数，这些函数都可以使用到函数组件中的，都有一个共同的特征，以use开头
    二、useState
    1、作用：让函数组件能操作状态
    2、步骤
    1）在react核心包中具名导入useState
    import {useState} from 'react'
    2) 在函数中执行useState(),
    返回值：
        返回一个数组类型的对象，这个对象的第1位就是state的变量,
        第二个参数是用来修改state的方法，相当于之前类组件中的setState
    参数：
        1）直接量：初始值
        2）回调函数：回调函数的返回值就是初始值
   
    对比：
        类组件
        this.state={
            num:0
        }
        函数组件
        const [num,setNum]=useState()
    3)使用

*/
export default function Counter() {
  //调用useState
  const [num,setNum]=useState(10)
  const [name,setName]=useState('Giles')
  const increment=()=>{
    setNum(num+1)
  }
  console.log('**********************');
  return (
    <div>
        <h1>计数器</h1>
        <h2>{num}</h2>
        <button onClick={increment}>+</button>
        <hr />
        {name}
        <button onClick={()=>{
            setName('Monica')
        }}>更新</button>
    </div>
  )
}
