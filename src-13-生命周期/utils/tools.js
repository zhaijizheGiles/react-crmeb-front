function isObject(obj){
    return obj!=null&&/^(object|function)$/.test(typeof obj)
}
export function shallowEqual(objA,objB){
    if(!isObject(objA)||!isObject(objB)) return false
    if(objA===objB) return true
    const keysA=Reflect.ownKeys(objA)
    const keysB=Reflect.ownKeys(objB)
    if(keysA.length!=keysB.length) return false
    for(let i=0;i<keysA.length;i++){
        let key=keysA[i]
        if(!objB.hasOwnProperty(key)||!Object.is(objA[key],objB[key])){
            return false
        }
    }
}
