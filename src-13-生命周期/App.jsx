import React, { Component } from 'react'
import {shallowEqual} from './utils/tools'
export default class App extends Component {
  constructor(){
    super()
    this.state={
      user:{
        name:'张三'
      }
    }
  }
  
  shouldComponentUpdate(nextProps,nextState){
    return shallowEqual(nextState.user,this.state.user)
  }
  render() {
    console.log('----------------');
    return (
      <div>
        <div>{this.state.user.name}</div>
        <button onClick={()=>{
          this.setState({
            user:{
              name:'里斯'
            }
          })
        }}>更改</button>
      </div>
    )
  }
}
