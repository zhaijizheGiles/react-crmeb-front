import React, { Component } from 'react'

export default class House extends Component {
  render() {
    return (
        <div>
        <div>楼盘信息</div>
        <table>
            <thead>
                <tr>
                    <td>编号</td>
                    <td>名称</td>
                    <td>价格</td>
                    <td>位置</td>
                    <td>电话</td>
                    <td>图片</td>
                </tr>
            </thead>
            <tbody>
                {
                    this.props.list.map(item=><tr key={item.id}>
                        <td>{item.id}</td>
                        <td>{item.name}</td>
                        <td>{item.price}</td>
                        <td>{item.address}</td>
                        <td>{item.tel}</td>
                        <td><img style={{
                            width:'80px',
                            height:'80px'
                        }} src={item.imgpath} alt="" /></td>
                    </tr>)
                }
            </tbody>
        </table>
    </div>
    )
  }
}
