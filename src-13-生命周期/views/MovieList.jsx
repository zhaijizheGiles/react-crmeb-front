import React, { Component } from 'react'
import axios from 'axios';
import House from './House';
import Student from './Student';
export default class MovieList extends Component {

  constructor(props){
    super(props)
    this.state={
        list:[],
        list1:[],
        type:1
    }
  }
  componentDidMount(){
    console.log('--------componentDidMount-----------');
    console.log('movieType',this.props.movieType);
  }
  UNSAFE_componentWillReceiveProps(nextProps){
    console.log('--------UNSAFE_componentWillReceiveProps-----------');
    console.log('movieType',nextProps.movieType);
    this.setState({
        type:nextProps.movieType
    })
    if(nextProps.movieType==1){
        this.getHouse()
    }else if(nextProps.movieType==2){
        this.getStudents()
    }
  }

  getHouse=async()=>{
    const result=await axios.get('https://www.fastmock.site/mock/aa707460627cc0c10a3e37f7925f6191/w06prj/getAllHouses')
    this.setState({
        list:result.data
    })
  }
  getStudents=async()=>{
    const result=await axios.get('https://www.fastmock.site/mock/aa707460627cc0c10a3e37f7925f6191/w06prj/getAllStudents')
    console.log('result',result);
    this.setState({
        list:result.data
    })
  }
  render() {
    return (
      <div>
        {this.state.type==1?<House list={this.state.list}></House>:<Student list1={this.state.list}></Student>}
      </div>
    )
  }
}
