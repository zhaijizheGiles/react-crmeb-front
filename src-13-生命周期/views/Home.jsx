import React, { Component } from 'react'
import MovieList from './MovieList'

export default class Home extends Component {
  constructor(props){
    super(props)
    this.state={
        movieType:1
    }
  }
  changeMovieType=(val)=>{
    this.setState({
        movieType:val
    })
  }
  render() {
    return (
      <div>
         <div>
            <button onClick={()=>{this.changeMovieType(1)}}>获取楼盘信息</button>
            <button onClick={()=>{this.changeMovieType(2)}}>获取学生信息</button>
         </div>
         <div>
            <MovieList movieType={this.state.movieType}></MovieList>
         </div>
      </div>
    )
  }
}
