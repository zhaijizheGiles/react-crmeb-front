import React, { Component } from 'react'

export default class LifeCycle extends Component {
  /*
    constructor的作用如下
    1、定义state的初始化数据
    2、对事件处理函数进行this指向的改变（bind方式）
    3、完成对props属性的校验和默认值的工作
    注意：
    1、在这个阶段中是不能访问DOM元素的
    2、此处可以获取到props传递进来的值，而且传递进来的props已经完成了验证和默认值的设置
    3、没有构造函数，如上也可以完成
  */
  constructor(props){
    super(props)
    console.log('-----1、constructor--------');
    this.titleRef=React.createRef()
    console.log('DOM元素:',this.titleRef.current);
    console.log('props值:',this.props.title);
    this.state={
      x:0,
      y:0,
      count:0
    }
  }
  //  componentWillMount(){
  //  console.log('-----2、componentWillMount--------');
  //  }
  /*
    在18.x之后对于建议过期钩子，必须使用UNSAFE_开头，如果在严格模式下将会报错
  */
  UNSAFE_componentWillMount(){
    console.log('-----2、componentWillMount--------');
    console.log('DOM元素:',this.titleRef.current);
  }
  /*
    componentDidMount中的作用
    1、完成对DOM的操作
    2、完成向后端发送ajax请求或者fetch请求
    3、完成原生事件的绑定
    4、定时器的设置
  */
  componentDidMount(){
    console.log('------4、componentDidMount-----------');
    console.log('DOM元素：',this.titleRef.current); 
    window.addEventListener('mousemove',this.moveMouseHandle)
    window.setInterval(()=>{
      this.setState({
        count:this.state.count+1
      })
    },1000)
  }
  static defaultProps={
    title:'默认演示'
  }
  moveMouseHandle=(e)=>{
    this.setState({
      x:e.clientX,
      y:e.clientY
    })
  }

  render() {
    console.log('------3、render-----------');
    console.log('DOM元素：',this.titleRef.current); 
    return (
      <div>
        <h1 ref={this.titleRef}>{this.props.title}</h1>
        <h2>{this.state.num}</h2>
        <div>
          X坐标为:{this.state.x},Y轴的坐标:{this.state.y}
        </div>
        <div style={{
          width:'100px',
          height:'100px',
          background:'springgreen',
          display:'flex',
          justifyContent:'center',
          alignItems:'center',
          fontSize:'38px',
          color:'white',
          borderRadius:'50%'
        }}>
          {this.state.count}
        </div>
      </div>
    )
  }
}
