import React, { Component } from 'react'
import ProductItem from './ProductItem'
import '../assets/style/bargain.scss'
export default class Bargain extends Component {
  render() {
    const { list} = this.props
    return (
      <div style={{ display: 'flex' }}>
        {list.map(item => <ProductItem key={item.id} {...item}>
          <div>{item.title}</div>
          <div className='kanjia'>砍价活动</div>
        </ProductItem>)}
      </div>
    )
  }
}
