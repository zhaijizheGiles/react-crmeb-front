import React, { Component } from 'react'
import '../assets/style/productItem.scss'
/*
    商品列表项
*/
export default class ProductItem extends Component {
  constructor(props){
    super(props)
  }
  render() {
    const {image,info,title,price,children}=this.props
    console.log('children',children);
    return (
      <div className='product-item'>
        <div className='item-header'>
          <img src={image} alt="" />
        </div>
        <div className='item-title'>{children[0]}</div>
        <div className='item-price'>￥{price}</div>
        {children[1]}
      </div>
    )
  }
}
