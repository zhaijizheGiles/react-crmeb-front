import React, { Component } from 'react'
import ProductItem from './ProductItem'

export default class Combination extends Component {
 
  render() {
    const {list}=this.props
    return (
      <div style={{display:'flex'}}>
         {list.map(item=><ProductItem key={item.id} {...item}>
            <div>{item.info}</div>
            <div style={{fontSize:'12px',color:'#999',textDecoration:'line-through'}}>￥{item.otPrice}</div>
         </ProductItem>)}
      </div>
    )
  }
}
