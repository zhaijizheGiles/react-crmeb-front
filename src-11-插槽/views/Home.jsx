import Combination from '@/components/Combination'
import React, { Component } from 'react'
import api from '@/api'
import Bargain from '@/components/Bargain'
export default class Home extends Component {
  constructor(props){
    super(props)
    this.state={
      combinationList:[],
      bargainList:[]
    }
  }
  componentDidMount(){
    this.getCombinationApi()
    this.getBargainApi()
  } 
  getCombinationApi=async()=>{
    const result=await api.product.combination()
    this.setState({
      combinationList:result.data.data.productList
    })
  }
  getBargainApi=async()=>{
    const result=await api.product.bargain()
    console.log('result',result.data.data.productList);
    this.setState({
      bargainList:result.data.data.productList
    })
  }
  render() {
    const {combinationList,bargainList}=this.state
    return (
      <div>
        <div>
          {combinationList.length>0?<Combination list={combinationList}></Combination>:<></>}
        </div>
        <div>
          {bargainList.length>0?<Bargain list={bargainList}></Bargain>:<></>}
        </div>
      </div>
    )
  }
}
