import React, { Component } from 'react'
import './EventBind2.scss'
export default class EventBind2 extends Component {
  componentDidMount(){
    document.querySelector("#aa").onclick=function(e){
        window.alert("Giles欢迎您")
        //return false
        console.log('giles',e);
        e.preventDefault()
    }
  }
  render() {
    return (
      <div className='grandFather'
           onClick={()=>{
             console.log('-----爷爷冒泡-------');
           }}
           onClickCapture={()=>{
            console.log('-----爷爷捕获-------');
           }}>
        <div className='father'
            onClick={()=>{
                console.log('-----爸爸冒泡-------');
            }}
            onClickCapture={()=>{
                console.log('-----爸爸捕获-------');
            }}>
            <div className='son'
                onClick={()=>{
                    console.log('-----儿子冒泡-------');
                }}
                onClickCapture={()=>{
                    console.log('-----儿子捕获-------');
                }}></div>
        </div>
        <div>
            <a href="http://www.woniuxy.com" onClick={(e)=>{
                window.alert('蜗牛学苑欢迎您！')
                console.log('woniuxy',e);
                e.preventDefault()
            }}>蜗牛学苑</a>
            <a href="http://www.zhaijizhe.cn" id='aa'>Giles</a>
        </div>
      </div>
    )
  }
}
