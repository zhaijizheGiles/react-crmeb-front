import React, { Component } from 'react'
/* 
    React将原生事件做了一次封装，封装以后的事件称为合成事件
    合成事件绑定方式
    1、onClick={function(){}}或者onClick={()=>{}} 这种写法页面结构变的混乱
    2、onClick={this.类中的普通方法} 存在this执行问题，需要通过bind方式来改变this指向
    3、onClick={this.类中的箭头函数} 不容易进行参数的传递，只能由一个合成事件的event对象
    4、onClick={()=>{this.箭头函数()}}:相对比较完美
    关于this指向
    1、如果你定义的是类中的成员方法，this执行改变可以在两个地方去写
     1）可以在构造函数中写：this.成员方法=this.成员方法.bind(this)
     2) 可以在render函数的调用的jsx中使用:onClick={this.成员方法.bind(this)}
    2、如果你定义的在render的函数，函数包括如下
    1）、定义的是声明式函数只能在调用处使用onClick={声明式函数名.bind(this)}
    2)、定义的是函数表达式只能在调用处使用onClick={声明式函数名.bind(this)}

*/
export default class EventBind extends Component {
  //类似于vue中mouted生命周期
  componentDidMount(){
    document.querySelector('#btn1').addEventListener('click',function(){
        console.log('--------原生事件------------');
    })
  }
  constructor(){
    super()
    this.clickbind1=this.clickbind1.bind(this)
  }
  //定义普通方法
  clickbind1(){
    console.log('------合成事件响应函数2-1--------',this);
  }
  //类中定义的方法以箭头函数形式存在
  //在调用的时候如果不传参，只是以函数名的形式调用，那么事件处理中的隐式参数传递的是一个合成事件的SyntheticBaseEvent 对象
  clickJiantou=(arg)=>{
    console.log('-------合成事件箭头函数方式-------------------',arg);
  }

  clickJiantouFinal=(arg1,arg2,arg3)=>{
    console.log('-------合成事件最终版--------------');
    console.log('arg1',arg1);
    console.log('arg2',arg2); 
    console.log('arg3',arg3);
  }

  render() {
    const clickbind2=function(){
        console.log('------合成事件响应函数2-2--------',this);
    }
    const clickbind3=()=>{
        console.log('------合成事件响应函数2-3--------',this);
    }
    function clickbind4(){
        console.log('------合成事件响应函数2-4--------');
    }
    return (
      <div>
          <button id='btn1'>原生事件调用</button>
          <button onClick={function(){
            console.log('合成事件绑定方式1-1');
          }}>合成事件绑定方式1-1</button>
          <button onClick={()=>{
            console.log('合成事件绑定方式1-2');
          }}>合成事件绑定方式1-2</button>
          <hr />
          <button onClick={this.clickbind1}>合成事件绑定方式2-1</button>
          <button onClick={clickbind2.bind(this)}>合成事件绑定方式2-2</button>
          <button onClick={clickbind3}>合成事件绑定方式2-3</button>
          <button onClick={clickbind4.bind(this)}>合成事件绑定方式2-4</button>
          <hr />
          <button onClick={this.clickJiantou}>箭头函数方式</button>
          <hr />
          <button onClick={(e)=>{this.clickJiantouFinal(e,10,20)}}>最终版</button>
      </div>
    )
  }
}
