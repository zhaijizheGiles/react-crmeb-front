/*
  一、类组件和函数组件的使用市场份额
  类组件的的使用越来越少
  React定义组件的方式有两种，类组件，函数组件
  React 16.8类组件有状态组件，函数组件无状态组件
  React 16.8之后引入hooks之后，函数组件也能操作状态了
  二、如何定义类组件
  1、使用ES6的class来定义类组件
  2、要让普通的class继承React.Component
  3、这个类组件中必须重写父类中的render方法
  4、这个render必须要有返回值，这个返回值是JSX
  5、这个类组件要被别的地方引入，必须使用export default进行默认导出

  三、关于类名
  1、类名是由字母数字下划线或者美元$符合组成
  2、类名不能以数字开头
  3、类名中间不能由空格
  4、类名符合大驼峰命名法
  5、见名知意
*/
import React,{Component} from 'react'
import EventBind from './views/EventBind2'
class App extends Component{
   render(){
     return (<>
        <EventBind/>
     </>)
   }
}
export default App
