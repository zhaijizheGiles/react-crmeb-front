//导入react-dom包
import ReactDOM from 'react-dom/client'
//导入自定义的js的方式使用相对路径的方式导入
import App from '@/App'
/*
    使用ReactDOM.createRoot函数创建root对象
    参数说明：要渲染到的真实DOM节点
*/
const root=ReactDOM.createRoot(document.querySelector('#root'))
//使用root对象中的render方法，将虚拟DOM渲染到页面中来
root.render(<App/>)


//React16版本的写法
//ReactDOM.render(<App/>,document.querySelector("#root"))

