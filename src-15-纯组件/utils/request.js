import axios from 'axios'

axios.defaults.baseURL="http://apif.java.crmeb.net"
axios.defaults.timeout=5000

axios.interceptors.request.use(config=>{
    return config
})
axios.interceptors.response.use(response=>{
    return response
},err=>{
    return Promise.reject(err)
})
export default axios