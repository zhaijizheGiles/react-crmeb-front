import React, { Component} from 'react'
import {shallowEqual} from '../utils/tools'
export default class LifeCycle2 extends Component {
  constructor(props){
    super(props)
    this.state={
        name:'张三'
    }
  }
  shouldComponentUpdate(nextProps,nextState){
    // console.log('this.state',this.state);
    // console.log('nextState',nextState);
    // console.log(shallowEqual(this.state,nextState));
    return !shallowEqual(this.state,nextState)

  }
  render() {
    console.log('------render------------');
    return (
      <div>
        <h1>{this.state.name}</h1>
        <button onClick={()=>{
            this.setState({
                name:'李四'
            })
        }}>更新</button>
      </div>
    )
  }
}
