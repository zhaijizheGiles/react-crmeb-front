import React, { Component, PureComponent } from 'react'
import {shallowEqual} from '../utils/tools'
export default class LifeCycle extends PureComponent {
  state={
    list:[1,2,3,4,5]
  }

  // shouldComponentUpdate(nextProps,nextState){
  //   return !shallowEqual(this.state,nextState)
  // }
  render() {
    return (
      <div style={{
        display:'flex'
      }}>
         {
          this.state.list.map(item=><div style={{
            width:'100px',
            height:'100px',
            background:'orange',
            borderRadius:'50%',
            display:'flex',
            justifyContent:'center',
            alignItems:'center',
            fontSize:'38px',
            color:'white',
            margin:'10px'
          }}key={item}>{item}</div>)
         }
         <div style={{
            width:'100px',
            height:'100px',
            border:'3px dashed #ccc',
            borderRadius:'50%',
            display:'flex',
            justifyContent:'center',
            alignItems:'center',
            fontSize:'38px',
            color:'#ccc',
            margin:'10px'
          }}
          onClick={()=>{
            this.state.list.push(6)
            this.setState({
              // list:[...this.state.list]
              list:new Array(...this.state.list)
            })
          }}>
            +
         </div>
      </div>
    )
  }
}
