import React, { Component } from 'react'
import LifeCycle from './views/LifeCycle'
export default class App extends Component {
  render() {
    return (
      <div>
        <LifeCycle></LifeCycle>
      </div>
    )
  }
}
