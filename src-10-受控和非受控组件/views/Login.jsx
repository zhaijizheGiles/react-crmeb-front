import React, { Component,createRef } from 'react'

export default class Login extends Component {

  constructor(props){
    super(props)
    //调用createRef()函数，创建ref对象
    this.usernameEle=createRef()
  }
  login=(e)=>{
    e.preventDefault()
    console.log('用户名:',this.usernameEle.current.value);

  }
  render() {
    return (
      <div>
         <form action="#" onSubmit={this.login}>
            <div>
                <label htmlFor="username">姓名:</label>
                <input type="text" id='username' ref={this.usernameEle}/>
            </div>
            <div>
                <input type="submit" value="提交"/>
            </div>
         </form>
      </div>
    )
  }
}
