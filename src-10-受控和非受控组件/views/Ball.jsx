import React, { Component,createRef } from 'react'
import '../assets/style/ball.scss'
export default class Ball extends Component {
 constructor(props){
    super(props)
    this.ballEle=createRef()
  }
  changeColor=()=>{
    this.ballEle.current.style.background="linear-gradient(springgreen,orange)"
  }
  render() {
    return (
      <div className='ball' onClick={this.changeColor} ref={this.ballEle}></div>
    )
  }
}
