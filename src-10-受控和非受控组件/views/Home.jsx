import React, { Component } from 'react'

export default class Home extends Component {
  constructor(props){
    super(props)
    this.state={
      user:{
        username:'Giles',
        password:'123456'
      }
    }
  }
  handleUserName=(e)=>{
    this.setState({
      user:{
        ...this.state.user,
        username:e.target.value
      }
    })
  }
  handlePassword=(e)=>{
    this.setState({
      user:{
        ...this.state.user,
        password:e.target.value
      }
    })
  }
  render() {
    return (
      <div>
         <div>
            <label htmlFor="username">姓名:</label>
            <input id="username" type="text" value={this.state.user.username} onChange={this.handleUserName}/>
         </div>
         <div>
           <label htmlFor="password">密码:</label>
           <input type="text" id='password' value={this.state.user.password} onChange={this.handlePassword}/>
         </div>
      </div>
    )
  }
}
