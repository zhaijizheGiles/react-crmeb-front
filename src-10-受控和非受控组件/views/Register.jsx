import React, { Component } from 'react'
/*
    第一步：在具体的表单项元素中,使用ref调用回调函数
*/
export default class Register extends Component {
  register=(e)=>{
    e.preventDefault()
    //将事件的默认行为去掉
    console.log('this',this.username.value);
     
  }
  render() {
    return (
      <div>
        <form action="#" onSubmit={this.register}>
            <div>
                <label htmlFor="usernmae">姓名:</label>
                <input type="text" id='username' ref={element=>this.username=element}/>
            </div>
            <div>
                <input type='submit' value="提交"></input>
            </div>
        </form>
      </div>
    )
  }
}
