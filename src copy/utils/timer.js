/**
 * 功能描述：用来完成两个时间的差计算
 * @param {*} target 目标实践对象
 * 返回值：返回对象包含时、分、秒属性
 */
export function diffTime(target){
    //创建当前时间
    let currentDate=new Date()
    let result=Math.ceil((target-currentDate)/1000)
    //计算出秒
    let seconds=result%60
    let minutes=parseInt(result%(60*60)/60)
    let hours=parseInt(result%(60*60*24)/(60*60))
    return {hours,minutes,seconds}
}