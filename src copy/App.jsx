import React, { useState,useEffect } from 'react'
import './App.scss'
import {diffTime} from '@/utils/timer'
export default function App() {

  console.log('--------render----------------------');
  const [hours,setHours]=useState(()=>{
    let result=Math.ceil((new Date('2023/4/21')-new Date())/1000)
    let hours=parseInt(result%(60*60*24)/(60*60))
    return hours
  })
  const [minuties,setMinuties]=useState(()=>{
    let result=Math.ceil((new Date('2023/4/21')-new Date())/1000)
    let minutes=parseInt(result%(60*60)/60)
    return minutes
  })
  const [seconds,setSeconds]=useState(()=>{
    let result=Math.ceil((new Date('2023/4/21')-new Date())/1000)
    let seconds=result%60
    return seconds
  })
  /**
   * 
   * 如果useEffect的第2个参数是一个[]数组，那么它就相当于类组件中的componentDidMount
   * 类组件生命周期
   * 挂载阶段
   * 1、constructor
   * 2、render
   * 3、componentDidMount
   * 运行阶段
   * render(new Props,setState,forceUpdate)
   */
  useEffect(()=>{
    let timers=window.setInterval(()=>{
      const {hours,minutes,seconds}=diffTime(new Date('2023/4/21'))
      console.log(hours,minutes,seconds);
      setHours(12)
      // setHours(hours)
      // setMinuties(minutes)
      // setSeconds(seconds)
    },1000)
    return ()=>{
      window.clearInterval(timers)
    }
  },[])

  return (
    <div className='container'>
      <div className="item">{hours}</div>
      <div className="item">{minuties}</div>
      <div className="item">{seconds}</div>
    </div>
  )
}
