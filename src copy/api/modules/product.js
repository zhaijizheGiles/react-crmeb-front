import request from '@/utils/request'
export default{
    combination:()=>request.get('/api/front/combination/index'),
    bargain:()=>request.get('/api/front/bargain/index')
}