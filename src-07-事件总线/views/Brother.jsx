import React, { Component } from 'react'
import MyEventBus from '@/utils/MyEventBus'
export default class Brother extends Component {
  sendMoneyToParent=()=>{
    /**
     * 通过事件总线发送信息的方法
     * emit(参数1,参数2
     * 参数1：自定义事件的名称
     * 参数2：要传递的数据
     * 
     */
    MyEventBus.emit('moneyBind','大哥给兄弟们500元')
  }  
  render() {
    return (
      <div style={{
        flexGrow:1,
        backgroundColor:'skyblue'
      }}>
        <h1>我是哥哥</h1>
        <button onClick={()=>{this.sendMoneyToParent()}}>发送200元</button>
      </div>
    )
  }
}
