import React, { Component } from 'react'
import Brother from './Brother'
import Sister from './Sister'

export default class Parent extends Component {
  render() {
    return (
      <div style={{
        width:'500px',
        height:'500px',
        backgroundColor:'yellow',
        display:'flex'
      }}>
          <Brother></Brother>
          <Sister></Sister>
      </div>
    )
  }
}
