import React, { Component } from 'react'
import MyEventBus from '@/utils/MyEventBus'
export default class Sister extends Component {
  state={
    msg:''
  }

  getBrotherMsg=(msg)=>{
    console.log('我是妹妹:',msg);
    this.setState(()=>{
      return{
        msg
      }
    })
  }
  //在此钩子函数中用来监听事件总线
  componentDidMount(){
    //用来监听事件总线上的方法
    /*
      addListener(参数1，参数2)
      参数1：自定义事件
      参数2： 回调函数
    */
    MyEventBus.addListener('moneyBind',this.getBrotherMsg)
  }

  componentWillUnmount(){
    MyEventBus.removeListener('moneyBind',this.getBrotherMsg)
  }
  constructor(props){
    super(props)
  }
  render() {
    return (
      <div style={{
        flexGrow:1,
        backgroundColor:'pink'
      }}>
        <h1>我是妹妹</h1>
        <span>{this.state.msg}</span>
      </div>
    )
  }
}
