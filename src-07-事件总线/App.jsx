import React, { Component } from 'react'
import Parent from './views/Parent'

export default class App extends Component {
  render() {
    return (
      <div>
          <Parent></Parent>
      </div>
    )
  }
}
