import React from 'react'
import '@/assets/styles/App2.scss'
export default function App2() {
  const orderList=[
    {
        icon:'icon-daifukuan1',
        title:'待付款'
    },
    {
        icon:'icon-daifahuo1',
        title:'待发货'
    },
    {
        icon:'icon-shouhou1',
        title:'待收货'
    },
    {
        icon:'icon-daipingjia1',
        title:'待评价'
    },
    {
        icon:'icon-daishouhuo1',
        title:'售后/退货'
    }
  ]
  return (
    <div className='container'>
        <div className='order-container'>
            <div className='order-container-top'>
                <div>订单中心</div>
                <div>查询更多</div>
            </div>
            <div className='order-container-body'>
                {
                    orderList.map((item,index)=><div key={index}>
                        <div><i className={`iconfont ${item.icon}`}></i></div>
                        <div className='order-title'>{item.title}</div>
                    </div>)
                }
            </div>
        </div>
    </div>
  )
}
