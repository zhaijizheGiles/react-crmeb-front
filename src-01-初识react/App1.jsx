import { getBirthday } from '@/utils/date'
import '@/assets/styles/App.scss'
// import logo from "./assets/image/logo.png"

export default () => {
    //定义一个对象
    const userInfo = {
        idcard: '610122198404084030',
        name: 'Giles',
        age: 39,
        gender: 1,
        avatar: 'https://api.java.crmeb.net/crmebimage/store/2020/08/15/adae23e354114cd5bd8f3cae740741c23opxeh8kw2.jpg'
    }
   
    const avatarBox = { width: '150px', height: '150px', backgroundColor: '#ccc', borderRadius: '50%', position: 'relative', boxShadow: '10px 10px 10px #999' }
    return (<>
        <div className='box'>
            <h1>个人简介</h1>
            <img src={require('./assets/image/logo.png')} alt="" />
            <div>姓名:{userInfo.name}</div>
            <div>年龄:{userInfo.age}</div>
            <div>性别:{userInfo.gender == 1 ? "男" : "女"}</div>
            <div>生日:{getBirthday(userInfo['idcard'])}</div>
            <div style={avatarBox}>
                <img src={userInfo.avatar} style={{ width: '100px', height: '100px', position: 'absolute', left: '50%', top: '50%', transform: 'translate(-50%,-50%)' }} />
            </div>
        </div>
    </>)
}