/* 
  函数组件要比普通的函数有如下约束
  - 必须要有一个返回值
  - 返回值就是JSX(构建虚拟DOM的方法)
  - 这里边必须要有一个根节点
  - 必须要将这个函数导出(使用ES6默认导出方式)
*/
import { getBirthday } from '@/utils/date'
export default function App() {
  //定义变量
  let name = "Giles"
  let age = 39
  let gender = 0
  let hobby = ['唱歌', '看书', '下棋']
  let idcard = '610122198404084030'
  let flag = false
  let books = [
    {
      isbn: '67876765',
      bookName: '《精通javascript编程》',
      price: 45.6,
      press: '人民邮电出版社'
    },
    {
      isbn: '67876764',
      bookName: '《精通Vue2》',
      price: 45.6,
      press: '清华大学出版社'
    },
    {
      isbn: '67876762',
      bookName: '《精通React》',
      price: 25.6,
      press: '北京大学出版社'
    }
  ]
  return (<>
    <h1>自我介绍</h1>
    <ul>
      <li>姓名:{name}</li>
      <li>年龄:{age}</li>
      <li>性别:{gender == 0 ? '男' : '女'}</li>
      <li>兴趣爱好:{hobby.join("、")}</li>
      <li>生日:{getBirthday(idcard)}</li>
      <li>是否成年:{age >= 18 ? '成年' : '未成年'}</li>
    </ul>
    {
      flag ? <button>隐藏</button> : <button>显示</button>
    }
    <hr />
    <table>
      <thead>
        <tr>
          <th>ISBN</th>
          <th>书名</th>
          <th>价格</th>
          <th>出版社</th>
        </tr>
      </thead>
      <tbody>
        {
          books.map((item, index) => <tr key={item.isbn}>
            <td>{item.isbn}</td>
            <td>{item.bookName}</td>
            <td>{item.price}</td>
            <td>{item.press}</td>
          </tr>
          )
        }
      </tbody>
    </table>

  </>)

}