import React,{useRef,useState} from 'react'
// 引-调-用

let aRef;
class Demo01 extends React.Component{
  constructor(props){
    super(props)
    this.state={
      num:0
    }
    
  }
  render(){
    const numRef=React.createRef()
    return (<>
      <h2 ref={numRef}>{this.state.num}</h2>
      <button onClick={()=>{
        this.setState({
          num:this.state.num+1
        })
        if(!aRef){
          console.log('****************');
          aRef=numRef
        }else{
          console.log('&&&&&&&&&&&&&&&&&&');
          console.log(aRef==numRef);
        }
      }}>+</button>
    </>)
  }
}
// let aRef;
// function Demo2(){
//   const [num,setNum]=useState(0)
//   const numRef=useRef()
//   return(<>
//     <h2 ref={numRef}>{num}</h2>
//     <button onClick={()=>{
//        setNum(num+1)
//        if(!aRef){
//         console.log('************');
//         aRef=numRef
//        }else{
//         console.log('$$$$$$$$$$');
//         console.log(aRef==numRef);
//        }
//     }}>+</button>
//   </>)
// }
/**
 * 在函数组件中通过React.createRef()每次执行完之后都会创建一个新的ref对象
 * 使用useRef可以让ref对象达到复用的效果，不会重新创建新的对象，这样对于性能来说是很后的提升
 * 
 * 在类组件中我们的createRef()对象的创建是在初始化中创建的对象，每次render不会重新创建，所以也不会影响性能
 * 
 */
// let aRef
// function Demo3(){
//   const [num,setNum]=useState(0)
//   let numRef=React.createRef()
//   return(<>
//      <h2 ref={numRef}>{num}</h2>
//      <button onClick={()=>{
//        setNum(num+1)
//        console.log('*******',aRef);
//        if(!aRef){
//         console.log('))))))');
//         aRef=numRef
//         console.log(aRef);
//       }else{
//         console.log('^^^^^^^^^^^^');
//         console.log(aRef==numRef)
//       }
//      }}>+</button>
//   </>)
// }
export default function App() {
  return (
    <div>
      <Demo01></Demo01>
      {/* <Demo2></Demo2> */}
      {/* <Demo3></Demo3> */}
    </div>
  )
}
