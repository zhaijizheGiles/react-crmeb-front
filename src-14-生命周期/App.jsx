import React, { Component } from 'react'
import Parent from './components/Parent'
// import LifeCycle2 from './components/LifeCycle2'


export default class App extends Component {
  constructor(props){
    super(props)
    this.state={
      flag:true
    }
  }
  render() {
    return (
      <div style={{
        width:'200px',
        height:'200px',
        backgroundColor:'springgreen'
      }}>
        {
          this.state.flag?<Parent></Parent>:<></>
        }
        <button onClick={()=>{
          this.setState({flag:!this.state.flag})
        }}>switch</button>
      </div>
     
    )
  }
}
