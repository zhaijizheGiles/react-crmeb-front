import React, { Component } from 'react'

export default class LifeCycle2 extends Component {
 constructor(props){
    super(props)
    this.state={
        num:0
    }
 }
 componentDidMount(){
    this.timers=window.setInterval(()=>{
        this.setState({
            num:this.state.num+1
        })
        console.log('#####',this.state.num);
    },1000)
 }
 componentWillUnmount(){
    window.clearInterval(this.timers)
 }
  render() {
    return (
      <div style={{
        width:'100px',
        height:'100px',
        backgroundColor:'orange'
      }}>
        {this.state.num}
      </div>
    )
  }
}
