import React, { Component } from 'react'
import Child from './Child';

export default class Parent extends Component {
    
 constructor(props){
    super(props)
    this.state={num:0}
    console.log('***********constructor**************');
  }
  componentDidMount(){
    console.log('***********componentDidMount**********');
  }
  componentWillUnmount(){
    console.log('***********componentWillUnmount**********');
  }
  render() {
    console.log('***********render*********************');
    return (
      <div>
         <button onClick={()=>{this.setState({num:this.state.num+1})}}>+</button>
        <Child num={this.state.num}></Child>
      </div>
    )
  }
}
