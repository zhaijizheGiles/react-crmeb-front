import React, { Component } from 'react'

export default class LifeCycle extends Component {
  constructor(props){
    super(props)
    this.state={
        name:'张三'
    }
  }
  shouldComponentUpdate(nextProps,nextState){
    console.log('------shouldComponentUpdate------------');
    // console.log('ths.state',this.state);
    // console.log('nextState',nextState);
    if(this.state.name!=nextState.name){
        return true
    }else{
        return false
    }
  }
  UNSAFE_componentWillUpdate(){
    console.log('------componentWillUpdate------------');
    console.log('this.state',this.state);
  }
  componentDidUpdate(){
    console.log('------componentDidUpdate------------');
    console.log('this.state',this.state);
  }
  render() {
    console.log('------render------------');
    return (
      <div>
        <h1>{this.state.name}</h1>
        <button onClick={()=>{
            this.setState({
                name:'李四'
            })
        }}>更新</button>
      </div>
    )
  }
}
