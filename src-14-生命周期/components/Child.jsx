import React, { Component } from 'react'

export default class Child extends Component {
  constructor(props){
    super(props)
    console.log('----------constructor----------');
  }
  UNSAFE_componentWillReceiveProps(){
    console.log('----------componentWillReceiveProps----');
  }
  shouldComponentUpdate(){
    console.log('----------shouldComponentUpdate----------');
    return true
  }
  componentDidUpdate(){
    console.log('----------componentDidUpdate----------');
  }
  componentWillUnmount(){
    console.log('----------componentWillUnmount----------');
  }
  render() {
    return (
      <div>Child</div>
    )
  }
}
