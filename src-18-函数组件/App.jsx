/**
 * 1、概念：使用普通函数(声明式或者函数表达式)或者箭头函数定义的组件称为函数组件
 * 2、定义函数组件的具体规则
 *   1）函数定义的组件名称建议首字母大写
 *   2）这个函数必须要有返回值
 *   3）return后面的内容就是JSX,建议使用()括号括起来
 *   4) 使用ES6的方式将其导出
 *  3、函数组件和类组件的不同点
 *   1）定义的方式不同：类组件使用ES6的class来完成定义的，函数组件是使用函数的方式来定义
 *   2）访问state的方式不同:类组件中可以使用state方式来操作状态，函数组件在react16.8之间只能渲染，不能操作状态，所么我们也将函数组称为无状态组件，类组件称为有状态组件
 *   3) 渲染方法不同:类组件通过render()方法来实现渲染，函数组件直接返回的就是JSX
 */

import Parent from "./components/Parent"

// import BindEventFunComponent from '@/components/BindEventFunComponent'
function App(){
  return (<>
    <h1>函数组件具体内容</h1>
   <Parent/>
  </>)
}
export default App