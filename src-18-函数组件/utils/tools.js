function isObject(arg){
    return arg!=null&&/^object|function$/.test(typeof arg)
}
export function shallowEqual(arg1,arg2){
    //判断传递进来的参数是否属于对象，如果不是对象则返回false
    if(!isObject(arg1)||!isObject(arg2)) return false
    if(arg1===arg2) return true
    //获取对象两个对象中的key，并以数组的形式返回
    let keys1=Object.keys(arg1)
    let keys2=Reflect.ownKeys(arg2)
    if(keys1.length!==keys2.length) return false   
    for(let i=0;i<keys1.length;i++){
        //获取到arg1对象中的属性值
       let arg1Key=keys1[i]
       if(!arg2.hasOwnProperty(arg1Key)||!Object.is(arg1[arg1Key],arg2[arg1Key])){
          console.log('两个对象中至少有一个key是不一样的或者属性的值不一样');
          return false
       }else{
          return true
       }
    }
}