import Child from "./Child"
export default ()=>{
    const getChildMsg=(val)=>{
        console.log('父组件:',val);
    }
    return (<div style={{
        width:'300px',
        height:'500px',
        backgroundColor:'yellow'
    }}>
        <h1>父组件</h1>
        <Child title={"学生信息"} cb={getChildMsg}></Child>
        <Child title={"教师信息"} cb={getChildMsg}></Child>
        <Child cb={getChildMsg}></Child>
    </div>)
}

