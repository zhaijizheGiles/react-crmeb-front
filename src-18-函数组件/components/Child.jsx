import React from 'react'
import PropTypes from 'prop-types';
export default function Child(props) {
  const {title}=props
  const handle=(arg)=>{
    props.cb("❤"+arg+"🐌")
  }
  return (
    <div 
        style={{
            width:'150px',
            height:'150px',
            backgroundColor:'skyblue'
    }}>
        <h3>{title}</h3>
        <button onClick={()=>{handle(title)}}>点击</button>
    </div>
  )
}
//函数组件的prpos默认值
Child.defaultProps={
    title:'默认标题'
}
//函数组件的验证规则
Child.propTypes={
    title:PropTypes.string.isRequired
}
