/*
    函数组件的事件处理有如下几种方式
    1、直接在标签中使用onXX={()=>{}}来完成
    2、调用当前函数组件中的内部方法(声明式、函数表达式、箭头函数)
*/
const BindEventFunComponent=function(){
    function bind1(){
        console.log('-----------事件处理函数绑定方式2-----------');
    }
    const bind2=function(){
        console.log('-----------事件处理函数绑定方式3-----------');
    }
    const bind3=(arg)=>{
        console.log('-----------事件处理函数绑定方式4-----------',arg);
    }
    const bind4=(arg1,arg2,arg3)=>{
        console.log('-----------事件处理函数绑定方式5-----------');
        console.log('arg1',arg1);
        console.log('arg2',arg2);
        console.log('arg3',arg3);
    }
    return(<>
        <h2>事件处理</h2>
        <button onClick={()=>{
            console.log('-----------事件处理函数绑定方式1----------');
        }}>按钮1</button>
        <button onClick={bind1}>按钮2</button>
        <button onClick={bind2}>按钮3</button>
        <button onClick={bind3}>按钮4</button>
        <button onClick={(e)=>{bind4(e,10,20)}}>按钮5</button>
    </>)
}
export default BindEventFunComponent