import React, { Component } from 'react'
// import LifeCycle from './views/LifeCycle1'

export default class App extends Component {
  constructor(){
    super()
    this.state={
      a:1
    }
  }
  render() {
    return (
      <div>
        <LifeCycle a={this.state.a}></LifeCycle>
        <button onClick={()=>{this.setState({a:this.state.a+1})}}>a+</button>
      </div>
    )
  }
}
