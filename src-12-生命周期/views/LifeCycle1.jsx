import React, { Component } from 'react'

export default class LifeCycle1 extends Component {
  constructor(props){
    super(props)
    this.state={
        num:0
    }
  }
//   UNSAFE_componentWillReceiveProps(nextProps){
//     console.log('--------componentWillReceiveProps---------');
//     console.log('props',this.props.a);
//     console.log('nextProps',nextProps.a);
//   }
  componentDidMount(){
   console.log('--------componentDidMount---------------'); 
   console.log('props',this.props.a);
  }
  /*
    如下三种情况render会被调用
    1、setState()
    2、this.forceUpdate()
    3、new Props
  */
  render() {
    console.log('------render--------');
    return (
      <div>
        <div>
            <h1>生命周期运行阶段</h1>
            <h2>{this.state.num}</h2>
            <h3>{this.props.a}</h3>
            <button onClick={()=>{this.setState(()=>({num:this.state.num+1}))}
            }>+</button>
            <button onClick={()=>{this.forceUpdate()}}>forceupdate</button>
        </div>
        
      </div>
    )
  }
}
