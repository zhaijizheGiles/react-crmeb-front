import React, { useMemo, useState } from 'react'

export default function AddCalc() {
  const [firstNum,setFirstNum]=useState(10)
  const [secondNum,setSecondNum]=useState(20)
  const resultNum=useMemo(()=>{
    return firstNum+secondNum
  },[firstNum,secondNum])
  return (
    <div>
        <div>
            第1个数:{firstNum}
            <button onClick={()=>setFirstNum(firstNum+1)}>+</button>
        </div>
        <div>第2个数:{secondNum}</div>
        <div>结果:{resultNum}</div>
    </div>
  )
}
