import React, { Component } from 'react'
import api from '@/api'
import './leaderboard.scss'
export default class Leaderboard extends Component {
  constructor(){
    super()
    this.state={
        list:[]
    }
  }

  async getLeaderboard(){
    const result=await api.product.leaderboard()
    this.setState(()=>{
        return{
            list:result.data.data
        }
    },()=>{
        console.log('result',this.state.list);
    })
    
  }
  componentDidMount(){
    this.getLeaderboard()
  }
  render() {
    const {list}=this.state
    return (
      <div className='leaderboard-container'>
          {
            list.map((item,index)=><div key={item.id} className='leaderboard-item'>
                <div className='leaderboard-img'>
                    <img src={item.image} alt="" />
                </div>
                <div>
                    {item.storeName}
                </div>
                <div>
                    ￥{item.price}
                </div>
            </div>)
          }
      </div>
    )
  }
}
