import React, { Component } from 'react'
import Leaderboard from '@/views/home/Leaderboard'
export default class Home extends Component {
  render() {
    return (
      <div>
        <Leaderboard/>
      </div>
    )
  }
}
