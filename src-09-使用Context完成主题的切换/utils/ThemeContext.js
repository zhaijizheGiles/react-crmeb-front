import {createContext} from 'react'
//定义主题数组
export const themes={
    "dark":{
        backgroundColor:'#000',
        color:'#fff',
    },
    "light":{
        backgroundColor:'#ccc',
        color:'#000'
    },
    "sky":{
        backgroundColor:'skyblue',
        color:'#000'
    },
    "green":{
        backgroundColor:'springgreen',
        color:'#fff'
    },
}

const context=createContext()
export default context