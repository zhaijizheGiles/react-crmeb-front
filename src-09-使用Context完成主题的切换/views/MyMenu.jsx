import React, { Component } from 'react'
import ThemeContext from '@/utils/ThemeContext'
import './menu.scss'
export default class MyMenu extends Component {
  render() {
  
    const {backgroundColor,color}=this.context
    console.log('backgroundColor',backgroundColor);
    console.log('color',color);
    return (
      <div className='menu' style={{backgroundColor,color}}>Menu</div>
    )
  }
}
MyMenu.contextType=ThemeContext
