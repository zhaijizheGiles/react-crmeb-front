import React, { Component } from 'react'
import './topheader.scss'
import ThemeContext,{themes} from '@/utils/ThemeContext'
export default class MyHeader extends Component {
  constructor(props){
    super(props)
    this.state={
      themesAry:[]
    }
  }
  componentDidMount(){
    this.getAryByObj(themes)
  }
  getAryByObj(themes){
    console.log('themes',themes);
    let ary=[]
    for(let key in themes){
      const item={...themes[key],key}
      ary.push(item)
    }
    this.setState(()=>{
      return{
        themesAry:ary
      }
    })
  }
  render() {
    const {backgroundColor,color,callback}=this.context
    const {themesAry}=this.state
    

    return (
      <div className='topheader' style={{backgroundColor,color}}>
          {
            themesAry.map((item,index)=><div 
                  className='blok' key={index} 
                  style={{backgroundColor:item.backgroundColor,color:item.color}}
                  onClick={()=>{
                    callback(item.key)
                  }}>√</div>)
          }
      </div>
    )
  }
}
MyHeader.contextType=ThemeContext