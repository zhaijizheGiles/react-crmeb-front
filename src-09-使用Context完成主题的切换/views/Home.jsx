import React, { Component } from 'react'
import './home.scss'
import ThemeContext,{themes} from '@/utils/ThemeContext'
import MyMenu from './MyMenu'
import MyHeader from './MyHeader'
import DashBoard from './DashBoard'
export default class Home extends Component {
  constructor(props){
    super(props)
    this.state={
      curTheme:themes['light']
    }
  }
  callbackTheme(arg){
    this.setState({
      curTheme:themes[arg]
    })
  }
  render() {
    const {curTheme}=this.state
    return (
      <ThemeContext.Provider value={{...curTheme,callback:this.callbackTheme.bind(this)}}>
        <div className='container'>
         <MyMenu></MyMenu>
          <div className='main'>
              <MyHeader></MyHeader>
              <div className='dashborad'>
                <DashBoard></DashBoard>
              </div>
          </div>
        </div>
      </ThemeContext.Provider>
    )
  }
}
